<?php

namespace App\Form\Type;

use App\Entity\AccountGroup;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AccountType extends AbstractType
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        $builder
            ->add('name', TextType::class)
            ->add('group', EntityType::class, [
                'class' => AccountGroup::class,
                'query_builder' => function (EntityRepository $er) use ($user) {
                    return $er->createQueryBuilder('g')
                        ->where('g.user = :user')
                        ->setParameter('user', $user);
                },
                'choice_label' => 'title',
                'choice_value' => 'id',
                'placeholder' => 'Choose a group'
            ])
            ->add('planning', CheckboxType::class, [
                'label' => 'Planning',
                'required' => false,
                'label_attr' => [
                    'for' => 'planningCheckbox',
                ],
                'attr' => [
                    'id' => 'planningCheckbox'
                ]
            ]);
    }
}
