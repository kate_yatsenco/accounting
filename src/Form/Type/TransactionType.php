<?php

namespace App\Form\Type;

use App\Form\EventListener\EditTransactionTypeListener;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class TransactionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', TextType::class, ['error_bubbling' => true]
            )
            ->add('amount', IntegerType::class, ['error_bubbling' => true]
            )
            ->add('type', ChoiceType::class,
                [
                    'choices' => [
                        'withdrawal (-)' => false,
                        'deposit (+)' => true
                    ],
                    'placeholder' => false,
                    'error_bubbling' => true
                ])
            ->add('notes', TextType::class, ['error_bubbling' => true])
            ->add('tags', TagsType::class, ['error_bubbling' => true])
            ->addEventSubscriber(new EditTransactionTypeListener());
    }
}
