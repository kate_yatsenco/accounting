<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @UniqueEntity(fields="username", message="Username already taken")
 */
class User implements UserInterface
{
    const DEFAULT_ROLES = ['ROLE_USER'];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $salt;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @var array $roles
     * @ORM\Column(name="roles", type="json_array")
     */
    private $roles = [];

    /**
     * Many Users have many Accounts
     * @var Collection|Account[]
     * @ORM\ManyToMany(targetEntity="Account")
     * @ORM\JoinTable(name="users_accounts",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="account_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $accounts;

    /**
     * One User has Many AccountGroups
     * @ORM\OneToMany(targetEntity="AccountGroup", mappedBy="user")
     */
    private $account_groups;

    public function __construct()
    {
        $this->isActive = true;
        $this->salt = md5(uniqid('', true));
        $this->accounts = new ArrayCollection();
        $this->account_groups = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param string $salt
     * @return User
     */
    public function setSalt($salt): User
    {
        $this->salt = $salt;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setRoles(array $roles)
    {
        $roles = array_diff($roles, self::DEFAULT_ROLES);

        $this->roles = $roles;
    }

    public function getRoles()
    {
        return array_merge(self::DEFAULT_ROLES, $this->roles);
    }

    /**
     * @param string $role
     * @return User
     */
    public function addRole(string $role): User
    {
        if (in_array($role, self::DEFAULT_ROLES)) {
            return $this;
        }
        $this->roles[] = $role;
        return $this;
    }

    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * @param string $username
     * @return User
     */
    public function setUsername(string $username): User
    {
        $this->username = strtolower($username);
        return $this;
    }

    /**
     * @param string $plainPassword
     * @return User
     */
    public function setPlainPassword(string $plainPassword): User
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return Collection|Account[]
     */
    public function getAccounts(): ?Collection
    {
        return $this->accounts;
    }

    /**
     * @param Collection|Account[] $accounts
     * @return User
     */
    public function setAccounts($accounts): User
    {
        $this->accounts = $accounts;
        return $this;
    }

    /**
     * @param Account $account
     * @return User
     */
    public function addAccount(Account $account): User
    {
        if (!$this->accounts->contains($account)) {
            $this->accounts->add($account);
        }
        return $this;
    }

    /**
     * @return Collection|Account[]
     */
    public function getAccountGroups(): ?Collection
    {
        return $this->account_groups;
    }

    /**
     * @param Collection|Account[] $account_groups
     * @return User
     */
    public function setAccountGroups($account_groups): User
    {
        $this->account_groups = $account_groups;
        return $this;
    }

    /**
     * @param AccountGroup $account_group
     * @return User
     */
    public function addAccountGroup(AccountGroup $account_group): User
    {
        if (!$this->account_groups->contains($account_group)) {
            $this->account_groups->add($account_group);
        }
        return $this;
    }

    /**
     * @param AccountGroup $account_group
     * @return User
     */
    public function removeAccountGroup(AccountGroup $account_group): User
    {
        if ($this->account_groups->contains($account_group)) {
            $this->account_groups->removeElement($account_group);
        }
        return $this;
    }
}
