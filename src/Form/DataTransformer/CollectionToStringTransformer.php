<?php

namespace App\Form\DataTransformer;

use App\Entity\Tag;
use App\Service\ColorGenerator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Form\DataTransformerInterface;

class CollectionToStringTransformer implements DataTransformerInterface
{
    /**
     * @var ManagerRegistry
     */
    private $doctrine;

    /**
     * @var ColorGenerator
     */
    private $colorGenerator;

    public function __construct($doctrine, $colorGenerator)
    {
        $this->doctrine = $doctrine;
        $this->colorGenerator = $colorGenerator;
    }

    /**
     * @inheritdoc
     */
    public function transform($value)
    {
        if ($value === null) {
            return '';
        }
        $tagNames = [];

        /** @var Tag $tag */
        foreach ($value as $tag) {
            $tagNames[] = $tag->getName();
        }
        return implode(', ', $tagNames);
    }

    /**
     * @inheritdoc
     */
    public function reverseTransform($value)
    {
        $names = explode(',', $value);

        $names = array_map(function ($name) {
            return trim(strtolower($name));
        }, $names);

        $repository = $this->getRepository();
        $em = $this->getEntityManager();

        $existingTags = $repository->findBy(['name' => $names]);
        $existingTagNames = [];
        foreach ($existingTags as $existingTag) {
            $existingTagNames[] = $existingTag->getName();
        }
        $newTagNames = array_diff($names, $existingTagNames);

        foreach ($newTagNames as $newTagName) {
            $tag = new Tag($this->colorGenerator);
            $tag->setName($newTagName);
            $em->persist($tag);
            $existingTags[] = $tag;
        }
        $em->flush();
        $tagsCollection = new ArrayCollection($existingTags);

        return $tagsCollection;
    }

    private function getEntityManager()
    {
        return $this->doctrine->getManagerForClass(Tag::class);
    }

    private function getRepository()
    {
        return $this->doctrine->getRepository(Tag::class);
    }
}
