<?php

namespace App\Entity;

use App\Service\ColorGenerator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="tag")
 */
class Tag
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $color;

    /**
     * Many Tags have many Transactions
     * @var Collection | Transaction[]
     * @ORM\ManyToMany(targetEntity="Transaction", inversedBy="tags")
     * @ORM\JoinTable("tags_transactions")
     */
    private $transactions;

    /**
     * Tag constructor.
     * @param ColorGenerator $colorGenerator
     */
    public function __construct($colorGenerator)
    {
        $this->transactions = new ArrayCollection();
        $this->color = $colorGenerator->get();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Tag
     */
    public function setName($name): Tag
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactions(): ?Collection
    {
        return $this->transactions;
    }

    /**
     * @param Transaction[]|Collection $transactions
     * @return Tag
     */
    public function setTransactions($transactions): Tag
    {
        $this->transactions = $transactions;
        return $this;
    }

    /**
     * @param $transaction
     * @return Tag
     */
    public function addTransaction($transaction): Tag
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions->add($transaction);
        }
        return $this;
    }

    /**
     * @param $transaction
     * @return Tag
     */
    public function removeTransaction($transaction): Tag
    {
        if ($this->transactions->contains($transaction)) {
            $this->transactions->removeElement($transaction);
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @param string $color
     * @return Tag
     */
    public function setColor(string $color): Tag
    {
        $this->color = $color;
        return $this;
    }
}
