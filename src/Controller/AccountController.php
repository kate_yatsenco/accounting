<?php

namespace App\Controller;

use App\Entity\Account;
use App\Entity\AccountGroup;
use App\Entity\Tag;
use App\Entity\Transaction;
use App\Entity\User;
use App\Form\Type\AccountGroupType;
use App\Form\Type\AccountType;
use App\Form\Type\EditUserType;
use App\Form\Type\LoginType;
use App\Form\Type\TransactionType;
use App\Service\ColorGenerator;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/")
 */
class AccountController extends Controller
{
    const ITEMS_PER_PAGE = 200;

    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @param AuthorizationCheckerInterface $authChecker
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function indexAction(Request $request,AuthorizationCheckerInterface $authChecker)
    {
        if(false === $authChecker->isGranted('ROLE_USER')){
            return $this->redirectToRoute('user_login');
        }

        $account = new Account();
        $accountForm = $this->createForm(AccountType::class, $account);
        $accountForm->handleRequest($request);

        $accountGroup = new AccountGroup();
        $accountGroupForm = $this->createForm(AccountGroupType::class, $accountGroup);
        $accountGroupForm->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        /** @var User[] $users */
        $users = $em->getRepository(User::class)->getNotAdminUsers();

        $accounts = $currentUser->getAccounts();
        /** @var AccountGroup[] $accountGroups */
        $accountGroups = $currentUser->getAccountGroups();

        $emptyGroups = [];
        $notEmptyGroups = [];
        foreach ($accountGroups as $group) {
            if ($group->getAccounts()->count() === 0) {
                $emptyGroups[] = $group;
            } else {
                $notEmptyGroups[] = $group;
            }
        }

        $accountWithoutGroups = [];
        foreach ($accounts as $ac) {
            if ($ac->getGroup() !== null) continue;
            $accountWithoutGroups[] = $ac;
        }


        if ($accountForm->isSubmitted() && $accountForm->isValid()) {

            $currentUser->addAccount($account);
            $em->persist($account);
            $em->flush();

            return $this->redirectToRoute('account_page', ['id' => $account->getId()]);

        } elseif ($accountGroupForm->isSubmitted() && $accountGroupForm->isValid()) {
            $currentUser->addAccountGroup($accountGroup);
            $accountGroup->setUser($currentUser);
            $em->persist($accountGroup);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render('homepage.html.twig', [
            'account_form' => $accountForm->createView(),
            'account_group_form' => $accountGroupForm->createView(),
            'account_groups' => $accountGroups,
            'empty_groups' => $emptyGroups,
            'not_empty_groups' => $notEmptyGroups,
            'accounts_without_groups' => $accountWithoutGroups,
            'users' => $users
        ]);
    }

    /**
     * @Route("/account/{id}", name="account_page")
     * @param Request $request
     * @param Account $account
     * @return Response
     */
    public function openAccountAction(Request $request, Account $account)
    {
        /** @var User $user */
        $transaction = new Transaction();
        $form = $this->createForm(TransactionType::class, $transaction);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {
            $tags = $form->get('tags')->getData();

            /** @var Tag[] $tags */
            for ($i = 0; $i < count($tags); $i++) {
                $tags[$i]->addTransaction($transaction);
            }

            $account->addTransaction($transaction);
            $transaction->setAccount($account);
            $transaction->setTags($tags);
            $transaction->setBalance($account->getBalance());

            $em->persist($transaction);
            $em->flush();
            return $this->redirectToRoute('account_page',['id'=>$account->getId()]);
        }
        $repository = $this->getDoctrine()
            ->getRepository(Transaction::class);
        $query = $repository->findByAccountQuery($account);

        $paginator = $this->get('knp_paginator');

        $page = $request->query->getInt('page', 1);
        /** @var SlidingPagination $pagination */
        $pagination = $paginator->paginate(
            $query,
            $page,
            self::ITEMS_PER_PAGE
        );
        if (0 === $page) { // page not defined in request->query
            // send user to last page by default
            $pagination->setCurrentPageNumber($pagination->getPageCount());
        }

        return $this->render('account/show.html.twig', [
            'form' => $form->createView(),
            'account' => $account,
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("edit/transaction/{id}",name="edit_transaction")
     * @param Request $request
     * @param Transaction $transaction
     * @return Response
     */
    public function editTransactionAction(Request $request, Transaction $transaction)
    {
        $form = $this->createForm(TransactionType::class, $transaction);
        $form->handleRequest($request);

        $account = $transaction->getAccount();
        if ($form->isSubmitted()) {

            $em = $this->getDoctrine()->getManagerForClass(Transaction::class);
            $em->flush();
            return $this->redirectToRoute('account_page', ['id' => $transaction->getAccount()->getId()]);
        }

        return $this->render('transaction/edit.html.twig', [
            'form' => $form->createView(),
            'transaction' => $transaction,
            'account' => $account
        ]);
    }

    /**
     * @Route("edit/account_group/{id}",name="edit_account_group")
     * @param Request $request
     * @param AccountGroup $accountGroup
     * @return Response
     */
    public function editAccountGroupAction(Request $request, AccountGroup $accountGroup)
    {
        $form = $this->createForm(AccountGroupType::class, $accountGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManagerForClass(AccountGroup::class);
            $em->flush();
            return $this->redirectToRoute('homepage', ['id' => $accountGroup->getId()]);
        }

        return $this->render('account_group/edit.html.twig', [
            'form' => $form->createView(),
            'group' => $accountGroup
        ]);
    }

    /**
     * @Route("edit/account/{id}",name="edit_account")
     * @param Request $request
     * @param Account $account
     * @return Response
     */
    public function editAccountAction(Request $request, Account $account)
    {
        $form = $this->createForm(AccountType::class, $account);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManagerForClass(AccountGroup::class);
            $em->flush();
            return $this->redirectToRoute('homepage', ['id' => $account->getId()]);
        }

        return $this->render('account/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("remove/group/{id}",name="remove_account_group")
     * @param Request $request
     * @param AccountGroup $group
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeGroupAction(Request $request, AccountGroup $group)
    {
        $accounts = $group->getAccounts();

        foreach ($accounts as $account) {
            $account->setGroup(null);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($group);
        $em->flush();

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("remove/user/{id}",name="remove_user")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeUser(Request $request)
    {
        $userId = $request->get('id');

        $em = $this->getDoctrine()->getManager();
        $targetUser = $em->getRepository(User::class)->findOneBy(['id' => $userId]);

        $em->remove($targetUser);
        $em->flush();

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("edit/user/{id}",name="edit_user")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function editUserAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $form = $this->createForm(LoginType::class);
        $form->handleRequest($request);

        $userId = $request->get('id');
        $em = $this->getDoctrine()->getManager();

        /** @var User $targetUser */
        $targetUser = $em->getRepository(User::class)->findOneBy(['id' => $userId]);

        if ($form->isSubmitted() && $form->isValid()) {

            $username = $form['_username']->getData();
            $plainPassword = $form['_password']->getData();
            $em = $this->getDoctrine()->getManagerForClass(AccountGroup::class);

            if ($username !== null) {
                $targetUser->setUsername($username);
            }
            if ($plainPassword !== null) {
                $password = $passwordEncoder->encodePassword($targetUser, $plainPassword);
                $targetUser->setPassword($password);
            }
            $em->flush();

            return $this->redirectToRoute('homepage');
        }
        return $this->render('user/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
