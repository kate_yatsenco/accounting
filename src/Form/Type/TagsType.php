<?php

namespace App\Form\Type;

use App\Form\DataTransformer\CollectionToStringTransformer;
use App\Service\ColorGenerator;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TagsType extends AbstractType
{
    private $doctrine;

    private $colorGenerator;

    public function __construct(ManagerRegistry $doctrine, ColorGenerator $colorGenerator)
    {
        $this->doctrine = $doctrine;
        $this->colorGenerator = $colorGenerator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(new CollectionToStringTransformer($this->doctrine, $this->colorGenerator));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'invalid_message' => 'error',
        ));
    }

    public function getParent()
    {
        return TextType::class;
    }
}
