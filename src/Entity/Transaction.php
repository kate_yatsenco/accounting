<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="TransactionRepository")
 * @ORM\Table(name="transaction")
 */
class Transaction
{
    /* minus */
    public const WITHDRAWAL = false;

    /* plus */
    public const DEPOSIT = true;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer",nullable=false)
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Description cannot be blank")
     */
    private $description;

    /**
     * @ORM\Column(type="text",nullable=true)
     */
    private $notes;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Amount cannot be blank")
     */
    private $amount;

    /**
     * @ORM\Column(type="boolean")
     */
    private $type;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $balance;

    /**
     * Many Transactions have One Account.
     * @var Account $account
     * @ORM\ManyToOne(targetEntity="account", inversedBy="transactions")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id")
     */
    private $account;

    /**
     * Many Transactions have many Tags
     * @var Collection | Tag[]
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", mappedBy="transactions",cascade={"all"})
     */
    private $tags;

    public function __construct()
    {
        $this->date = new \DateTime();
        $this->tags = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Transaction
     */
    public function setDescription(string $description = null): Transaction
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getNotes(): ?string
    {
        return $this->notes;
    }

    /**
     * @param string $notes
     * @return Transaction
     */
    public function setNotes(string $notes = null): Transaction
    {
        $this->notes = $notes;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): ?int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return Transaction
     */
    public function setAmount(int $amount): Transaction
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return bool
     */
    public function getType(): ? bool
    {
        return $this->type;
    }

    /**
     * @param bool $type
     * @return Transaction
     */
    public function setType(bool $type): Transaction
    {
        if ($type === true) {
            $this->type = self::DEPOSIT;
        } else {
            $this->type = self::WITHDRAWAL;
        }
        return $this;
    }

    /**
     * @return int
     */
    public function getBalance(): ?int
    {
        return $this->balance;
    }

    /**
     * @param int $balance
     * @return Transaction
     */
    public function setBalance($balance): Transaction
    {
        $this->balance = $balance;
        return $this;
    }

    /**
     * @return Account
     */
    public function getAccount(): ?Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     * @return Transaction
     */
    public function setAccount(Account $account): Transaction
    {
        $this->account = $account;
        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): ?Collection
    {
        return $this->tags;
    }

    /**
     * @param Tag[]|Collection $tags
     * @return Transaction
     */
    public function setTags(Collection $tags): Transaction
    {
        foreach ($this->tags as $tag) {
            if (!$tags->contains($tag)) {
                $this->tags->removeElement($tag);
                $tag->removeTransaction($this);
            }
        }

        foreach ($tags as $tag) {
            if (!$this->tags->contains($tag)) {
                $this->tags->add($tag);
                $tag->addTransaction($this);
            }
        }

        return $this;
    }
}
