<?php

namespace App\Service;

use App\Entity\Tag;
use Doctrine\Common\Persistence\ManagerRegistry;

class ColorGenerator
{
    /**
     * @var ManagerRegistry
     */
    private $doctrine;

    private $lastColor = null;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    private $colors = [
        "#3CB371", "#8B4513", "#0000CD", "#9932CC", "#6495ED", "#9ACD32", "#1E90FF", "#D2B48C", "#DB7093", "#00CED1", "#696969",
        "#87CEFA", "#0DDFF9", "#90EE90", "#FFA07A", "#7B68EE", "#40E0D0", "#5F9EA0", "#A52A2A", "#BC8F8F", "#66CDAA", "#A9A9A9",
        "#0000FF", "#228B22", "#DDA0DD", "#00008B", "#FF00FF", "#2E8B57", "#A0522D", "#663399", "#EE82EE", "#E9967A", "#8B0000",
        "#B8860B", "#191970", "#87CEEB", "#008B8B", "#FF7F50", "#DA70D6", "#4682B4", "#8B008B", "#FFA500", "#8A2BE2", "#9400D3",
        "#999", "#778899", "#C0C0C0", "#8000", "#D2691E", "#FA8072", "#C71585", "#808000", "#20B2AA", "#E6470C", "#E29749",
        "#FFA6B6", "#B22222", "#9370DB", "#BA55D3", "#556B2F", "#3CDADA", "#DAA520", "#F9C83F", "#54E452", "#E62B2B", "#708090",
        "#F32D98", "#90BFBF", "#808080", "#2EE4A7", "#C2C2FB", "#FF00FF", "#ADD8E6", "#DC143C", "#6FDE6F", "#CD5C5C", "#FF69B4",
        "#B0C4DE", "#F9A5B2", "#F08080", "#6A5ACD", "#4B0082", "#8FBC8F", "#D69FD6", "#FF8C00", "#800080", "#00BFFF", "#32CD32",
        "#6B8E23", "#F59A4D", "#800000", "#CD853F", "#483D8B", "#2F4F4F", "#B7AE3C", "#DEB887", "#F36F58", "#48D1CC", "#999"];

    public function getColorsCount()
    {
        return count($this->colors);
    }

    public function get()
    {
        if (null === $this->lastColor) {
            $lastTag = $this->doctrine->getRepository(Tag::class)->findOneBy([], ['id' => 'DESC']);

            if (null === $lastTag) {
                $this->lastColor = $this->colors[0];
            } else {
                $this->lastColor = $lastTag->getColor();
            }
        }

        if ($this->lastColor === end($this->colors)) {
            $this->lastColor = $this->colors[0];
        } else {
            $lastIndex = array_search($this->lastColor, $this->colors);
            $this->lastColor = $this->colors[$lastIndex + 1];
        }
        return $this->lastColor;
    }
}
