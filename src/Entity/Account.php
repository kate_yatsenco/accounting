<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="account")
 */
class Account
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $planning;

    /**
     * One Account has Many Transactions.
     * @var Collection|Transaction[] fields
     * @ORM\OneToMany(targetEntity="Transaction", mappedBy="account")
     */
    private $transactions;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $balance = 0;

    /**
     * Many Accounts have One Group.
     * @ORM\ManyToOne(targetEntity="AccountGroup", inversedBy="accounts")
     * @ORM\JoinColumn(name="account_group_id", referencedColumnName="id", nullable=true)
     */
    private $group;

    public function __construct()
    {
        $this->transactions = new ArrayCollection();
    }

    // todo add custom validation, name unique per user

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Account
     */
    public function setName(string $name): Account
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function getPlanning(): ?bool
    {
        return $this->planning;
    }

    /**
     * @param bool $planning
     * @return Account
     */
    public function setPlanning(bool $planning): Account
    {
        $this->planning = $planning;
        return $this;
    }

    /**
     * @return Collection| Transaction[]
     */
    public function getTransactions(): ?Collection
    {
        return $this->transactions;
    }

    /**
     * @param ArrayCollection $transactions
     * @return Account
     */
    public function setTransactions(ArrayCollection $transactions): Account
    {
        $this->transactions = $transactions;
        return $this;
    }

    /**
     * @param Transaction $transaction
     * @return Account
     */
    public function addTransaction(Transaction $transaction): Account
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions->add($transaction);
        }
        if ($this->planning) {
            $this->balance = null;
        } else {
            if ($transaction->getType() === false) {
                $this->balance = $this->balance - $transaction->getAmount();
            } else {
                $this->balance = $this->balance + $transaction->getAmount();
                $transaction->setBalance($this->balance);
            }
        }
        return $this;
    }

    /**
     * @return integer
     */
    public function getBalance(): ?int
    {
        return $this->balance;
    }

    /**
     * @param int $balance
     * @return Account
     */
    public function setBalance($balance): Account
    {
        $this->balance = $balance;
        return $this;
    }

    /**
     * @return AccountGroup
     */
    public function getGroup(): ?AccountGroup
    {
        return $this->group;
    }

    /**
     * @param AccountGroup $group
     * @return Account
     */
    public function setGroup($group): Account
    {
        if ($group === null) {
            $this->group = null;
        }
        $this->group = $group;
        return $this;
    }
}
