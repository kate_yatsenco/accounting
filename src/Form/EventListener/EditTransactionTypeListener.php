<?php

namespace App\Form\EventListener;

use App\Entity\Transaction;
use App\Form\Type\TagsType;
use function PHPSTORM_META\type;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class EditTransactionTypeListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [FormEvents::PRE_SET_DATA => 'onPreSetData'];
    }

    public function onPreSetData(FormEvent $event)
    {
        /** @var Transaction $transaction */
        $transaction = $event->getData();
        $form = $event->getForm();

        if ($transaction->getId()) {
            $form
                ->add('date', TextType::class, [
                    'data' => $transaction->getDate()->format('m/d/y'),
                    'mapped' => false
                ])
                ->add('type', TextType::class, [
                    'attr' => [
                        'readonly' => 'readonly',
                    ],
                    'data' => ($transaction->getType()) ? 'deposit' : 'withdrawal',
                    'mapped' => false
                ])
                ->add('balance', TextType::class, [
                ]);

            if ($transaction->getAccount()->getPlanning()) {
                $form
                    ->add('type', ChoiceType::class,
                        [
                            'choices' => [
                                'withdrawal (-)' => false,
                                'deposit (+)' => true
                            ],
                            'placeholder' => false,
                            'error_bubbling' => true
                        ]);
            }

        }

    }
}
