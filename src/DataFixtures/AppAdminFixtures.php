<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppAdminFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $admin = new User;
        $plainPassword = 'Drg3fegTerf346hd';
        $admin->setUsername('admin');

        $admin->setPassword($this->encoder->encodePassword($admin, $plainPassword));
        $admin->addRole('ROLE_ADMIN');

        $manager->persist($admin);
        $manager->flush();
    }
}
