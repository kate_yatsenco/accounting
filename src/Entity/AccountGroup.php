<?php

namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="account_group")
 */
class AccountGroup
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * One Group has Many Accounts.
     * @ORM\OneToMany(targetEntity="Account", mappedBy="group")
     */
    private $accounts;

    /**
     * Many AccountGroups have One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="account_groups")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    public function __construct()
    {
        $this->accounts = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return AccountGroup
     */
    public function setTitle(string $title): AccountGroup
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return Collection | Account[]
     */
    public function getAccounts(): ?Collection
    {
        return $this->accounts;
    }

    /**
     * @param mixed $accounts
     * @return AccountGroup
     */
    public function setAccounts($accounts): AccountGroup
    {
        $this->accounts = $accounts;
        return $this;
    }

    /**
     * @param Account $account
     * @return AccountGroup
     */
    public function addAccount(Account $account): AccountGroup
    {
        if (!$this->accounts->contains($account)) {
            $this->accounts->add($account);
        }
        return $this;
    }

    /**
     * @param Account $account
     * @return AccountGroup
     */
    public function removeAccount(Account $account): AccountGroup
    {
        if ($this->accounts->contains($account)) {
            $this->accounts->removeElement($account);
        }
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return AccountGroup
     */
    public function setUser(User $user): AccountGroup
    {
        $this->user = $user;
        return $this;
    }

    public function getTotalAccountGroupBalance()
    {
        $balance = 0;
        /** @var Account $account */
        foreach ($this->accounts as $account) {
            $balance += $account->getBalance();
        }
        return $balance;
    }

}
